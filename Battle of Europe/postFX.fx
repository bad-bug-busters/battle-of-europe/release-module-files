////////////////////////////////////////////////////////////////////////////////////
//POST EFFECT SHADERS........ 

#include "fx_configuration.h"

//#define ENABLE_EDITOR
//#define USE_CHARACTER_SHADOW_MERGE

float4 output_gamma;	// vectorize?
float4 output_gamma_inv;

static const float3 LUMINANCE_WEIGHTS = float3(0.299f, 0.587f, 0.114f);
static const float min_exposure = 0.0f;
static const float max_exposure = 2.0f;

#pragma warning(disable: 3571)		//pow(f,e) warning!

#define ERROR_OUT(c) c = float4(texCoord.x * 10 - floor(texCoord.x * 10) > 0.5, texCoord.y * 10 - floor(texCoord.y * 10) > 0.5, 0, 1)

// use postFX_sampler4 for point sampling
#if defined(USE_FX_STATE_MANAGER) && !defined(USE_DEVICE_TEXTURE_ASSIGN)	//else we can use direct device access with sampler indexes...
	texture postFX_texture0, postFX_texture1, postFX_texture2, postFX_texture3, postFX_texture4;
	//non-srgb samplers
	sampler postFX_sampler0 : register(s0) = sampler_state	{  Texture = postFX_texture0;  };	// scene
	sampler postFX_sampler1 : register(s1) = sampler_state	{  Texture = postFX_texture1;  };	// hdr-low = 1/2res high = 1/4res
	sampler postFX_sampler2 : register(s2) = sampler_state	{  Texture = postFX_texture2;  };	// brightness .. AVG and MAX
	sampler postFX_sampler3 : register(s3) = sampler_state	{  Texture = postFX_texture3;  };	// black
	sampler postFX_sampler4 : register(s4) = sampler_state	{  Texture = postFX_texture4;  };	// == sampler 2 + WithLuminance???
	sampler postFX_sampler5 : register(s5);// depth
	// sampler postFX_sampler6 : register(s6);// cubic  ...black screen
	// sampler postFX_sampler7 : register(s7);// shadowmap ...white screen
	// sampler postFX_sampler8 : register(s8);// screen ...half resolution screen
	// sampler postFX_sampler9 : register(s9);// mesh ...shows random texture
	// sampler postFX_sampler10 : register(s10);// clamped ...black screen
	// sampler postFX_sampler11 : register(s11);// font ...shows font texture
	// sampler postFX_sampler12 : register(s12);// char shadow ...black screen
	// sampler postFX_sampler13 : register(s13);// mesh no filter ...shows random texture without filter
	// sampler postFX_sampler14 : register(s14);// diffuse no wrap ...black screen
	// sampler postFX_sampler15 : register(s15);// grass ...black screen
	
	// sampler postFX_sampler16 : register(s0);//reflection ...shows blurry screen
	// sampler postFX_sampler17 : register(s1);//env ...shows env texture
	// sampler postFX_sampler18 : register(s2);//diffuse ...shows random diffuse texture
	// sampler postFX_sampler19 : register(s3);//normal ...shows random normal texture
#else 
	#ifdef USE_REGISTERED_SAMPLERS
		sampler postFX_sampler0 : register(s0);	//linear clamp
		sampler postFX_sampler1 : register(s1);	//linear clamp
		sampler postFX_sampler2 : register(s2);	//linear clamp
		sampler postFX_sampler3 : register(s3);	//linear clamp
		sampler postFX_sampler4 : register(s4);	//linear clamp
	#else
		sampler postFX_sampler0 : register(s0) = sampler_state{ AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	 };	//linear clamp
		sampler postFX_sampler1 : register(s1) = sampler_state{ AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	 };	//linear clamp
		sampler postFX_sampler2 : register(s2) = sampler_state{ AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	 };	//linear clamp
		sampler postFX_sampler3 : register(s3) = sampler_state{ AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	 };	//linear clamp
		sampler postFX_sampler4 : register(s4) = sampler_state{ AddressU = CLAMP; AddressV = CLAMP; MinFilter = LINEAR; MagFilter = LINEAR;	 };	//linear clamp
	#endif
#endif

static const int clip_distance = 10000;// is 1250 by default you can change it in module.ini ...=> far_plane_distance = 10000 #was 1250 #clip distance
static const int num_iterations = 8;
static const float BlurPixelWeight[num_iterations] = {1.000, 0.875, 0.750, 0.625, 0.500, 0.375, 0.250, 0.125};

bool showing_ranged_data = false;

float4 g_HalfPixel_ViewportSizeInv;
float  g_HDR_frameTime;	

float g_DOF_Focus = -0.005;
float g_DOF_Range = 5.19876;

#ifndef PS_2_X
	#define PS_2_X ps_2_b
#endif

#define RELATIVE_PS_TARGET ps_2_X//MOD

#define HDRRange 				(postfxParams1.x)
#define HDRExposureScaler 		(postfxParams1.y)
#define LuminanceAverageScaler 	(postfxParams1.z)
#define LuminanceMaxScaler 		(postfxParams1.w)

#define BrightpassTreshold 	(postfxParams2.x)
#define BrightpassPostPower (postfxParams2.y)
#define BlurStrenght 		(postfxParams2.z)
#define BlurAmount 			(postfxParams2.w)


#define HDRRangeInv 		(1.0f / HDRRange)

float CalculateWignette(float2 tc) 
{
	tc = tc - 0.5; // [-1/2, 1/2]
	return pow(1-dot(tc,tc), 4);
}
float4 radial(sampler2D tex, float2 texcoord, int samples, float startScale = 1.0, float scaleMul = 0.9)
{
    float4 c = 0;
    float scale = startScale;
    for(int i=0; i<samples; i++) 
		{
        float2 uv = ((texcoord-0.5)*scale)+0.5;
        float4 s = tex2D(tex, uv);
        c += s;
        scale *= scaleMul;
    }
    c /= samples;
    return c;
}
float vignette(float2 pos, float inner, float outer)
{
  //float r = length(pos); //orj
  float r = dot(pos,pos);
  r = 1.0 - smoothstep(inner, outer, r);
  return r;
}

/////////////////////////////////////////////////////////////////////////////////////
struct VS_OUT_POSTFX
{
	float4 Pos:	POSITION;
	float2 Tex:	TEXCOORD0;
};
VS_OUT_POSTFX vs_main_postFX(float4 pos: POSITION)
{
	VS_OUT_POSTFX Out;

	Out.Pos = pos;
	Out.Tex = (float2(pos.x, -pos.y) * 0.5f + 0.5f) + g_HalfPixel_ViewportSizeInv.xy;
	
	return Out;
}
VertexShader vs_main_postFX_compiled = compile vs_2_0 vs_main_postFX();

/////////////////////////////////////////////////////////////////////////////////////
float4 ps_main_postFX_Show(float2 texCoord: TEXCOORD0) : COLOR 
{
	float4 color = tex2D(postFX_sampler0, texCoord);
	
	if(showing_ranged_data) 
	{
		color.rgb *= HDRRange;
		color.rgb = pow(color.rgb, output_gamma_inv);
	}
	return color;
}
technique postFX_Show
{
	pass P0
	{
		VertexShader = vs_main_postFX_compiled;
		PixelShader = compile ps_2_0 ps_main_postFX_Show();
	}
}

/////////////////////////////////////////////////////////////////////////////////////


// MOD no effect
#ifdef USE_CHARACTER_SHADOW_MERGE
float4 ps_main_postFX_Shadowmap(float2 texCoord: TEXCOORD0) : COLOR 
{
	float original_shadowmap = tex2D(postFX_sampler0, texCoord).r;
	float character_shadow = tex2D(postFX_sampler1, texCoord).r;
	return min(original_shadowmap, character_shadow);
}

technique shadowmap_updater{pass P0{
	VertexShader = vs_main_postFX_compiled;
	PixelShader = compile ps_2_0 ps_main_postFX_Shadowmap();}}
#endif
/////////////////////////////////////////////////////////////////////////////////////
// MOD seems not to do anything?
float4 color_value;

float4 ps_main_postFX_TrueColor(float2 texCoord: TEXCOORD0) : COLOR
{
	const bool use_vignette = true;
	
	float4 ret = color_value;
	if(use_vignette)
		ret.a = saturate(ret.a + ret.a * (1.0f - vignette(float2(texCoord.x*2-1, texCoord.y*2-1)*0.5f, 0.015f, 1.25f)) );	//remove blur from center
	return ret;
}

technique postFX_TrueColor{pass P0{
	VertexShader = vs_main_postFX_compiled;
	PixelShader = compile ps_2_0 ps_main_postFX_TrueColor();}}

/////////////////////////////////////////////////////////////////////////////////////

float4 ps_main_brightPass(float2 texCoord: TEXCOORD0 ) : COLOR0 
{
	//bloom
	float3 color = tex2D(postFX_sampler0, texCoord) * 127;//make it real values.. more accuracy

	//SSAO
	float occlusion = 0.0f;
 	float base_depth = tex2D(postFX_sampler5, texCoord).r;
	if (base_depth < 1.0f)//1 = clip_distance
	{
		base_depth *= clip_distance;
		
		int num_samples = 8;
		float radius = 2.0f;
		
		float2 normal;//now generate pseudo random values
		normal.x = frac(sin((texCoord.x + 0.5) * (texCoord.y + 0.5) * 1000.0f) * 1000.0f);
		normal.y = frac(cos((texCoord.x + 0.5) * (texCoord.y + 0.5) * 1000.0f) * 1000.0f);
		normal *= 2.0f;
		normal -= 1.0f;
		float2 offset = g_HalfPixel_ViewportSizeInv.zw * normal * 128 * radius / base_depth;
		
		float2 cur_offset;
		float difference_1, difference_2, check_dist;
		for(int i = 1; i <= num_samples; i++)
		{
			check_dist = radius / i;
			cur_offset = offset / i;
			difference_1 = base_depth - tex2D(postFX_sampler5, texCoord + cur_offset).r * clip_distance;
			difference_2 = base_depth - tex2D(postFX_sampler5, texCoord - cur_offset).r * clip_distance;
			// if one sample distance is invalid then use its twins inverse ... almost completely removes haloing
			if (difference_1 > check_dist && difference_2 < check_dist)
				difference_1 = -difference_2;
			else if (difference_2 > check_dist && difference_1 < check_dist)
				difference_2 = -difference_1;
			difference_1 = saturate(difference_1 / check_dist);
			difference_2 = saturate(difference_2 / check_dist);
			occlusion += saturate((difference_1 * (1.0f - difference_1) + difference_2 * (1.0f - difference_2)) - 0.25f);
		}
		occlusion /= num_samples;
		occlusion *= 16;
	}
	return float4(color, occlusion);
}

technique postFX_brightPass{pass P0{
	VertexShader = vs_main_postFX_compiled;
	PixelShader = compile PS_2_X ps_main_brightPass();}}
technique postFX_brightPass_WithLuminance{pass P0{
	VertexShader = vs_main_postFX_compiled;
	PixelShader = compile PS_2_X ps_main_brightPass();}}

/////////////////////////////////////////////////////////////////////////////////////

float4 ps_main_blurX(float2 inTex: TEXCOORD0) : COLOR0 
{
	float4 total_color = tex2D(postFX_sampler0, inTex) * BlurPixelWeight[0];
	float4 cur_color;
	float2 offset = float2(g_HalfPixel_ViewportSizeInv.z, 0);
	float2 cur_offset;
	float base_depth = tex2D(postFX_sampler5, inTex).r * clip_distance * 10.0f;
	float cur_amount;
	float2 blur_amount = 1.0f;
	for(int i = 1; i < num_iterations; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			if(j == 0)
				cur_offset = inTex + offset * i;
			else
				cur_offset = inTex - offset * i;
			cur_color = tex2D(postFX_sampler0, cur_offset) * BlurPixelWeight[i];
			cur_amount = max(1.0f / i, 1.0f - abs(base_depth - tex2D(postFX_sampler5, cur_offset).r * clip_distance * 10.0f));//based on difference
			total_color.a += cur_color.a * cur_amount;
			total_color.rgb += cur_color.rgb;
			blur_amount.x += BlurPixelWeight[i];
			blur_amount.y += BlurPixelWeight[i] * cur_amount;
		}
	}
	total_color.rgb /= blur_amount.x;
	total_color.a /= blur_amount.y;
	return total_color;
}
float4 ps_main_blurY(float2 inTex: TEXCOORD0) : COLOR0 
{
	float4 total_color = tex2D(postFX_sampler0, inTex) * BlurPixelWeight[0];
	float4 cur_color;
	float2 offset = float2(0, g_HalfPixel_ViewportSizeInv.w);
	float2 cur_offset;
	float base_depth = tex2D(postFX_sampler5, inTex).r * clip_distance * 10.0f;
	float cur_amount;
	float2 blur_amount = 1.0f;
	for(int i = 1; i < num_iterations; i++)
	{
		for(int j = 0; j < 2; j++)
		{
			if(j == 0)
				cur_offset = inTex + offset * i;
			else
				cur_offset = inTex - offset * i;
			cur_color = tex2D(postFX_sampler0, cur_offset) * BlurPixelWeight[i];
			cur_amount = max(1.0f / i, 1.0f - abs(base_depth - tex2D(postFX_sampler5, cur_offset).r * clip_distance * 10.0f));//based on difference
			total_color.a += cur_color.a * cur_amount;
			total_color.rgb += cur_color.rgb;
			blur_amount.x += BlurPixelWeight[i];
			blur_amount.y += BlurPixelWeight[i] * cur_amount;
		}
	}
	total_color.rgb /= blur_amount.x;
	total_color.a /= blur_amount.y;
	return total_color;
}

technique postFX_blurX{pass P0{
	VertexShader = vs_main_postFX_compiled;
	PixelShader = compile PS_2_X ps_main_blurX();}}
technique postFX_blurY{pass P0{
	VertexShader = vs_main_postFX_compiled;
	PixelShader = compile PS_2_X ps_main_blurY();}}

/////////////////////////////////////////////////////////////////////////////////////
//initial luminance calculation step
float4 ps_main_postFX_Average(float2 texCoord: TEXCOORD0) : COLOR 
{	
	static const float Offsets[4] = {-1.5f, -0.5f, 0.5f, 1.5f};
	float _max = 0;
	float _log_sum = 0;
	
	for (int x = 0; x < 4; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			float2 vOffset = float2(Offsets[x], Offsets[y]) * float2(g_HalfPixel_ViewportSizeInv.y, g_HalfPixel_ViewportSizeInv.w);
			float3 color_here = tex2D(postFX_sampler0, texCoord + vOffset).rgb;
			float lum_here = dot(color_here * HDRRange, LUMINANCE_WEIGHTS);
			
			_log_sum += /*log*/(lum_here/*+ 0.0000001f*/);
			_max = max(_max, lum_here);
		}
	}
	return float4(_log_sum / 16, _max, 0, 1);
}

technique postFX_Average{pass P0{
	VertexShader = vs_main_postFX_compiled;
	PixelShader = compile PS_2_X ps_main_postFX_Average();}}

float4 ps_main_postFX_AverageAvgMax(float2 texCoord: TEXCOORD0, uniform const bool smooth) : COLOR 
{
	static const float Offsets[4] = {-1.5f, -0.5f, 0.5f, 1.5f};
	float _max = 0;
	float _sum = 0;
	
	//downsample and find avg-max luminance
	for (int x = 0; x < 4; x++)
	{
		for (int y = 0; y < 4; y++)
		{
			float2 vOffset = float2(Offsets[x], Offsets[y]) * float2(g_HalfPixel_ViewportSizeInv.y, g_HalfPixel_ViewportSizeInv.w);
			float2 lumAvgMax_here = tex2D(postFX_sampler0, texCoord + vOffset).rg;
			
			_sum += lumAvgMax_here.r * lumAvgMax_here.r;
			_max = max(_max, lumAvgMax_here.g);
		}
	}
	float _avg = _sum / 16; 
	float4 new_ret = float4(sqrt(_avg), _max, 0, 1);
	
	if(smooth)
	{
		//last step,  finish average luminance calculat+ion
		new_ret.r = /*exp*/(new_ret.r);
		float2 prev_avgmax = tex2D(postFX_sampler4, float2(0.5f, 0.5f)).rg;
		//new_ret.xy = lerp(prev_avgmax, new_ret, /*1.0f); //*/g_HDR_frameTime );/***/
		new_ret.x = lerp(prev_avgmax.x, new_ret.x, g_HDR_frameTime );
		new_ret.y = max(0.1f, lerp(prev_avgmax.y, new_ret.y, g_HDR_frameTime ) );
	}
	return new_ret;
}

technique postFX_AverageAvgMax{pass P0{
	VertexShader = vs_main_postFX_compiled;
	PixelShader = compile PS_2_X ps_main_postFX_AverageAvgMax(false);}}
technique postFX_AverageAvgMax_Smooth{pass P0{
	VertexShader = vs_main_postFX_compiled;
	PixelShader = compile PS_2_X ps_main_postFX_AverageAvgMax(true);}}

/////////////////////////////////////////////////////////////////////////////////////
struct VsOut_Convert_FP2I 
{
	float4 Pos:			POSITION;
	float2 texCoord0:	TEXCOORD0;
	float2 texCoord1:	TEXCOORD1;
	float2 texCoord2:	TEXCOORD2;
	float2 texCoord3:	TEXCOORD3;
};
VsOut_Convert_FP2I vs_main_postFX_Convert_FP2I(float4 pos: POSITION)
{
	VsOut_Convert_FP2I Out;
	Out.Pos = pos;
	// Texture coordinates
	float2 texCoord = (float2(pos.x, -pos.y) * 0.5f + 0.5f) + g_HalfPixel_ViewportSizeInv.xy;
	Out.texCoord0 = texCoord + float2(-1.0,  1.0) * g_HalfPixel_ViewportSizeInv.xy;
	Out.texCoord1 = texCoord + float2( 1.0,  1.0) * g_HalfPixel_ViewportSizeInv.xy;
	Out.texCoord2 = texCoord + float2( 1.0, -1.0) * g_HalfPixel_ViewportSizeInv.xy;
	Out.texCoord3 = texCoord + float2(-1.0, -1.0) * g_HalfPixel_ViewportSizeInv.xy;
	return Out;
}

float4 ps_main_postFX_Convert_FP2I(float2 texCoord0: TEXCOORD0, float2 texCoord1: TEXCOORD1, float2 texCoord2: TEXCOORD2, float2 texCoord3: TEXCOORD3) : COLOR0 
{
	float3 rt;
	#define gamma_corrected_input
	#ifdef gamma_corrected_input
		rt  = tex2D(postFX_sampler4, texCoord0).rgb;
		rt += tex2D(postFX_sampler4, texCoord1).rgb;
		rt += tex2D(postFX_sampler4, texCoord2).rgb;
		rt += tex2D(postFX_sampler4, texCoord3).rgb;
	#else
		rt  = pow(tex2D(postFX_sampler4, texCoord0).rgb, output_gamma);
		rt += pow(tex2D(postFX_sampler4, texCoord1).rgb, output_gamma);
		rt += pow(tex2D(postFX_sampler4, texCoord2).rgb, output_gamma);
		rt += pow(tex2D(postFX_sampler4, texCoord3).rgb, output_gamma);
	#endif
	rt *= 0.25;
	//rt = BrightPass(rt);
	rt *= HDRRangeInv;
	return float4(rt.rgb,1);
}

technique postFX_Convert_FP2I{pass P0{
		VertexShader = compile vs_2_0 vs_main_postFX_Convert_FP2I();
		PixelShader = compile ps_2_0 ps_main_postFX_Convert_FP2I();}}

/////////////////////////////////////////////////////////////////////////////////////
float4 ps_main_postFX_DofBlur(uniform const bool using_hdr, uniform const bool using_depth, float2 texCoord: TEXCOORD0) : COLOR 
{
	float3 sample_start = tex2D(postFX_sampler0, texCoord).rgb;
	float depth_start;
	if(using_depth)
		depth_start = tex2D(postFX_sampler1, texCoord).rgb;
	
	static const int SAMPLE_COUNT = 8;
	static const float2 offsets[SAMPLE_COUNT] = {-1,-1, 0,-1, 1,-1,-1, 0, 1, 0,-1, 1, 0, 1, 1, 1};
	
	float sampleDist = g_HalfPixel_ViewportSizeInv.x * 3.14f;
	float3 sample = sample_start;
	
	for (int i = 0; i < SAMPLE_COUNT; i++) 
	{
		float2 sample_pos = texCoord + sampleDist * offsets[i];
		
		// !using_hdr -> non-lineer gamma!
		float3 sample_here;
		if(using_depth) 
		{	
			float depth_here = tex2D(postFX_sampler1, sample_pos).r;
			if(depth_here < depth_start)
				sample_here = sample_start;
			else
				sample_here = tex2D(postFX_sampler0, sample_pos).rgb;
		}
		else
			sample_here = tex2D(postFX_sampler0, sample_pos).rgb;
		sample += sample_here;
	}
	sample /= SAMPLE_COUNT+1;
	//sample.rgb =  pow(sample, input_gamma); 
	//sample.rgb = pow(sample.rgb, output_gamma_inv);
	//return pow(tex2D(postFX_sampler0, texCoord), input_gamma);
	return float4(sample.rgb, 1);
}

technique postFX_DofBlurHDR{pass P0{
		VertexShader = vs_main_postFX_compiled;
		PixelShader = compile ps_2_0 ps_main_postFX_DofBlur(true, false);}}
technique postFX_DofBlurLDR{pass P0{
		VertexShader = vs_main_postFX_compiled;
		PixelShader = compile ps_2_0 ps_main_postFX_DofBlur(false, false);}}
technique postFX_DofBlurHDR_Depth{pass P0{
		VertexShader = vs_main_postFX_compiled;
		PixelShader = compile ps_2_0 ps_main_postFX_DofBlur(true, true);}}
technique postFX_DofBlurLDR_Depth{pass P0{
		VertexShader = vs_main_postFX_compiled;
		PixelShader = compile ps_2_0 ps_main_postFX_DofBlur(false, true);}}

/////////////////////////////////////////////////////////////////////////////////////

float4 FinalScenePassPS(uniform const bool use_dof, uniform const int use_hdr, uniform const bool use_auto_exp, float2 texCoord: TEXCOORD0) : COLOR 
{
	// Sample the scene
	float4 color = tex2D(postFX_sampler0, texCoord);
 	if(use_hdr > 0) 
	{
		float3 bloom = pow(tex2D(postFX_sampler1, texCoord).rgb, 1);
		// float gray = (bloom.r + bloom.g + bloom.b) / 3.0f;
		// color.rgb += (bloom + (bloom.r + bloom.g + bloom.b)/3.0f);
		color.rgb *= 1 - bloom;// darken areas with lot of bloom
		color.rgb += bloom;
		/*color.rgb = lerp(bloom, color.rgb, 0.5f);*/
	}
	
	if(use_auto_exp)//tonemap
		color.rgb *= 1.5f - tex2D(postFX_sampler2, 0.5f).r;//r= avg g= max
	
	//gamma correction
	color.rgb = pow(color.rgb, output_gamma_inv + 0.55f);//make sure default gamma 2.2 is 1.0
	return color;
	//return 1.0f - tex2D(postFX_sampler1, texCoord).a;
}

//postFX_final_[dof]_[hdr_quality]_[auto_exposure]
technique postFX_final_0_0_0{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS( false, 0, false);	} }
technique postFX_final_0_1_0{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS( false, 1, false);	} }
technique postFX_final_0_2_0{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS( false, 2, false);	} }
technique postFX_final_0_1_1{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS( false, 1, true);	} }
technique postFX_final_0_2_1{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS( false, 2, true);	} }
technique postFX_final_1_0_0{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS(  true, 0, false);	} }
technique postFX_final_1_1_0{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS(  true, 1, false);	} }
technique postFX_final_1_2_0{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS(  true, 2, false);	} }
technique postFX_final_1_1_1{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS(  true, 1, true);	} }
technique postFX_final_1_2_1{	pass P0	{   VertexShader = vs_main_postFX_compiled;		PixelShader = compile PS_2_X FinalScenePassPS(  true, 2, true);	} }
//Recycle Bin: